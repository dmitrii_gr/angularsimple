import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";

import { AppComponent } from './app.component';
import { CarComponent } from './components/car/car.component';
import { ContactsComponent } from './components/contacts/contacts/contacts.component';

const appRouters: Routes=[
  {
    path: '', component: CarComponent,
  },
  {
    path: 'contacts', component: ContactsComponent,
  }


]

@NgModule({
  declarations: [
    AppComponent,
    CarComponent,
    ContactsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRouters)
  ],
  providers: [],
  bootstrap: [AppComponent, CarComponent]
})
export class AppModule { }
