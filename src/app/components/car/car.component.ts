
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  name: string | undefined;
  speed: number | undefined;
  model: string | undefined;

  colors: Colors | undefined;
  options: string[] | undefined;

  isEdit = false;
  constructor() {
  }

  ngOnInit() {
    this.carSelect();
  }

  showEdit() {
    this.isEdit = !this.isEdit;
  }
  carSelect(carName = 'BMW') {
    if (carName == 'BMW') {
      this.name = 'BMW';
      this.speed = 300;
      this.model = 'M5';
      this.colors = {
        car: 'Черный',
        salon: 'Леопардовый',
        wheels: 'Коричневый',
      };
      this.options = ['abs', 'avtopilot', 'parking Bmw']
    } else if (carName == 'AUDI') {
      this.name = 'AUDI';
      this.speed = 350;
      this.model = 'RS8';
      this.colors = {
        car: 'Белый',
        salon: 'Красный',
        wheels: 'Голубой',
      };
      this.options = ['drift', 'parking Audi']
    } else if (carName == 'MERS') {
      this.name = 'MERS';
      this.speed = 500;
      this.model = 'gelenvagen';
      this.colors = {
        car: 'золотой',
        salon: 'Оранжевый',
        wheels: 'Зеленый',
      };
      this.options = ['camping', 'abs', 'avtopilot', 'parking mers']
    }
  }

  addOpt(option = 'default') {

    this.options?.unshift(option);
    return false;
  }

  deleteOption(option: string) {
    for (let i = 0; i < this.options!.length; i++) {
      if (this.options![i] == option) {
        this.options?.splice(i, 1)
        break;
      }
    }
  }
}

interface Colors {
  car: string,
  salon: string,
  wheels: string;
}
